$(document).ready(function(){
    $("[popup-box]").wrapInner("<div popup-box-inner></div>");

    $(".ok").insertAfter($("[popup-box-inner]"))

    $("img[chara-id]").each(function(){
        $(this).wrap("<div chara-id='" + $(this).attr("chara-id")  + "'></div>");
        $(this).parent("[chara-id]").attr("og-img-url",$(this).attr("src"))
        $(this).parent("[chara-id]").css("background-image","url(" + $(this).attr("src") + ")");
        
        if($(this).is("[bg-pos]")){
            let bg_pos = $(this).attr("bg-pos");
            // $(this).parent("[chara-id]").css("background-position",bg_pos)
        }
        
        if($(this).is("[size]")){
            let bg_size = $(this).attr("size");
            // $(this).parent("[chara-id]").css("background-size",bg_size)
        }
        
        if($(this).is("[chara-id]")){
            let chara_id = $(this).attr("chara-id");
            $(this).parent("[chara-id]").append("<div chara-overlay>" + chara_id + "</div>")
        }
        
        $(this).remove()
    })

    $("h2 + [chara-id]").each(function(){
        $(this).nextUntil("hr").add($(this)).wrapAll("<div chara-grid></div>")
    })

    $("[chara-grid]").each(function(){
        if($(this).prev("h2").length){
            let gridH2Name = $(this).prev("h2").text();
            $(this).find("[chara-id]").each(function(){
                $(this).attr("chara-unit",gridH2Name)
            })
        }
    })

    /*--------------------*/

    $("[mc-holder]:has([empty-chara])").click(function(){
        $("[chara-select-popup]").fadeIn(400).css("display","flex")
    })

    /*--------------------*/

    $("[chara-id]").click(function(){
        $("[chara-id]").removeClass("c-selected");
        $(this).addClass("c-selected");
        
        if($(".ok").css("display") == "none"){
            $(".ok").fadeIn(250).css("display","flex")
        }
        
    })

    /*--------------------*/

    $(".ok button").click(function(){
        $("[chara-select-popup]").fadeOut(400);
        
        setTimeout(() => {
            let chosenName = $(".c-selected").attr("chara-id");
            let chosenPic = $(".c-selected").attr("og-img-url");
            let chosenUnit = $(".c-selected").attr("chara-unit");
            let chosenStyle = $(".c-selected").attr("style");
            
            /********************/
            
            if(chosenName == "miku"){
                $("[sc-group]:not(:first)").remove();
                $("[sc-holder] [empty-chara]").empty().append('<i class="ti ti-heart"></i>');
                $("[sc-name]").text("Affinity with all!");
                $("[sc-unit]").text("");
                $("[sc-group]:first").show()
            }
            
            /********************/
            
            if(chosenName == "rin"){
                clearExisting();
                addKizuna("miku");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");
                addKizuna("kaito");			
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("an");
                addKizuna("kaito");
                addKizuna("toya");
                
                addKizuna("nene");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "len"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("honami");
                
                addKizuna("haruka");
                addKizuna("airi");
                
                addKizuna("kohane");
                addKizuna("an");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("emu");
                addKizuna("rui");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "luka"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("honami");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("an");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("nene");
                addKizuna("rui");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "meiko"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("honami");
                addKizuna("shiho");
                
                addKizuna("haruka");
                addKizuna("airi");
                
                addKizuna("kohane");
                addKizuna("an");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("airi");
                addKizuna("nene");
                
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "kaito"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("emu");
                addKizuna("nene");
                addKizuna("rui");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
            }
            
            /********************/
            
            if(chosenName == "ichika"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("saki");
                addKizuna("honami");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                
                addKizuna("tsukasa");
                addKizuna("nene");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
            }
            
            /********************/
            
            if(chosenName == "saki"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("honami");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("emu");

                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "honami"){
                clearExisting();
                addKizuna("miku");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("shiho");
                
                addKizuna("haruka");
                addKizuna("shizuku");
                
                addKizuna("tsukasa");
                addKizuna("emu");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
                addKizuna("ena");
            }
            
            /********************/
            
            if(chosenName == "shiho"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("honami");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                
                addKizuna("tsukasa");
            }
            
            /********************/
            
            if(chosenName == "minori"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("shiho");
                
                addKizuna("haruka");
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("an");
                
                addKizuna("tsukasa");
                
                addKizuna("kanade");
            }
            
            /********************/
            
            if(chosenName == "haruka"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("honami");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("airi");			
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("an");
                
                addKizuna("emu");
            }
            
            /********************/
            
            if(chosenName == "airi"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("meiko");
                
                addKizuna("saki");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("an");
                addKizuna("akito");
                
                addKizuna("emu");
                
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "shizuku"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("honami");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("airi");
                
                addKizuna("kohane");
                addKizuna("an");
                
                addKizuna("tsukasa");
                
                addKizuna("mafuyu");
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "kohane"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("an");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("tsukasa");
            }
            
            /********************/
            
            if(chosenName == "an"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");
                
                addKizuna("minori");
                addKizuna("haruka");
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("nene");
                
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "akito"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("airi");
                
                addKizuna("kohane");
                addKizuna("an");
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("nene");
                addKizuna("rui");
                
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "toya"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("saki");
                
                addKizuna("kohane");
                addKizuna("an");
                addKizuna("akito");
                
                addKizuna("tsukasa");
                addKizuna("nene");
                addKizuna("rui");
                
                addKizuna("mizuki");			
            }
            
            /********************/
            
            if(chosenName == "tsukasa"){
                clearExisting();
                addKizuna("miku");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("saki");
                addKizuna("honami");
                addKizuna("shiho");
                
                addKizuna("minori");
                addKizuna("shizuku");
                
                addKizuna("kohane");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("emu");
                addKizuna("nene");
                addKizuna("rui");
                
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "emu"){
                clearExisting();
                addKizuna("miku");
                addKizuna("len");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("saki");
                addKizuna("honami");
                
                addKizuna("haruka");
                addKizuna("airi");
                
                addKizuna("tsukasa");
                addKizuna("nene");
                addKizuna("rui");
                
                addKizuna("mafuyu");
                addKizuna("ena");
            }
            
            /********************/
            
            if(chosenName == "nene"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("meiko");
                addKizuna("kaito");
                
                addKizuna("ichika");
                
                addKizuna("an");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("emu");
                addKizuna("rui");
            }
            
            /********************/
            
            if(chosenName == "rui"){
                clearExisting();
                addKizuna("miku");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("emu");
                addKizuna("nene");
                
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "kanade"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("honami");
                
                addKizuna("minori");
                
                addKizuna("mafuyu");
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "mafuyu"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("kaito");
                
                addKizuna("ichika");
                addKizuna("honami");
                
                addKizuna("shizuku");
                
                addKizuna("emu");
                
                addKizuna("kanade");
                addKizuna("ena");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "ena"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("luka");
                addKizuna("meiko");
                
                addKizuna("honami");
                
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("akito");
                
                addKizuna("emu");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
                addKizuna("mizuki");
            }
            
            /********************/
            
            if(chosenName == "mizuki"){
                clearExisting();
                addKizuna("miku");
                addKizuna("rin");
                addKizuna("len");
                addKizuna("luka");
                addKizuna("meiko");

                addKizuna("saki");
                
                addKizuna("airi");
                addKizuna("shizuku");
                
                addKizuna("an");
                addKizuna("akito");
                addKizuna("toya");
                
                addKizuna("tsukasa");
                addKizuna("rui");
                
                addKizuna("kanade");
                addKizuna("mafuyu");
                addKizuna("ena");
            }
            
            
            /********************/

            $("[mc-holder] [has-chara]").attr("style",chosenStyle);
            $("[mc-holder] [has-chara]").css("opacity","1")
            
            $("[mc-holder] [has-chara-overlay]").text(chosenName);
            
            /********************/
            
            $("[sc-col]").addClass("show");
            $(".hide-same-units").show();
            
            if(chosenName == "miku"){
                $(".hide-same-units").hide()
            }
            
            $("body, html").animate({scrollTop: 0},420);
        },0)
        
        
    })

    /*--------------------*/

    function clearExisting(){
        $("[sc-group]:not(:first)").remove();
        $("[sc-holder] [empty-chara]").empty().append('<i class="ti ti-question-mark"></i>');
        $("[sc-name]").text("");
        $("[sc-unit]").text("");
    }

    /*--------------------*/

    function addKizuna(persona){
        let kizuPic = $("[chara-id][chara-id='" + persona + "']").attr("og-img-url");
        let kizuStyle = $("[chara-id][chara-id='" + persona + "']").attr("style");
        let kizuUnit = $("[chara-id][chara-id='" + persona + "']").attr("chara-unit");
        // console.log(kizuPic)
        
        let scs = $("[sc-group]:first").clone();
        scs.find("[has-chara]").attr("style",kizuStyle);
        scs.find("[has-chara]").css("opacity","1");
        
        scs.find("[sc-name]").text(persona);
        scs.find("[sc-unit]").text(kizuUnit);
        
        $("[sc-group]:first").hide();
        $("[sc-col]").append(scs);
        
        scs.show();
    }

    /*--------------------*/

    $(".hide-same-units").click(function(){
        let currentChara = $("[mc-holder] [has-chara-overlay]").text();
        let currentUnit = $("[chara-id='" + currentChara + "']").attr("chara-unit");
        
        // show members from same unit	
        if(!$(".custom-toggle").hasClass("move-on")){
            $(".custom-toggle").addClass("move-on");
            
            $("[sc-unit]").each(function(){
                if($(this).text() == currentUnit){
                    $(this).parents("[sc-group]").fadeIn(300)
                }
            })
            
            $(".tog-text").text($(".tog-text").text().replace("Hiding","Showing"))
        }
        
        // hide members from same unit
        else {
            $(".custom-toggle").removeClass("move-on");
            
            $("[sc-unit]").each(function(){
                if($(this).text() == currentUnit){
                    $(this).parents("[sc-group]").fadeOut(300)
                }
            })
            
            $(".tog-text").text($(".tog-text").text().replace("Showing","Hiding"))
        }
    })

    $("button.credits").click(function(){
        $(".popup-box").find(".pp-inner").animate({scrollTop: 0},1)
    })
})