#### PJSekai Kizuna Rank Checker

**Author:** HT (@ glenthemes)  
**Region:** JP  
**Last updated:** 2024-03-09, 5:48PM [GMT-8]

> "Affinity" (or "trust") between <b>two characters</b> can be accumulated by setting a <b>leader</b> and a <b>sub-leader</b> for your team. This tool helps you check the list of available friendships. By leveling up trust ranks, you get valuable resources and rewards.

🔗 [**pjsk-kizuna-checker.gitlab.io/view**](https://pjsk-kizuna-checker.gitlab.io/view)